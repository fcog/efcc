<?php
/*
Template Name: Home
*/
?>
<?php get_header(); ?>

    <div id="content" class="grid_16">

<?php 
        // Query Arguments
        $args = array(
            'post_type' => 'post',
            'posts_per_page' => 5,
            'category_name' => 'featured',
        );  
 
        // The Query
        $the_query = new WP_Query( $args );

        if ( $the_query->have_posts() ):
 ?>
    
        <div id="slideshow" class="flexslider">
            <ul class="slides">
            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <li>
                    <div class="image">
                        <?php if (has_post_thumbnail()): ?>
                            <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_id() ), 'large' ); ?>
                            <img src="<?php echo $image[0] ?>">
                        <?php endif ?>
                    </div>
                    <div class="link">  
                         <a href="<?php echo get_permalink(); ?>">Read More</a>
                    </div>
                    <div class="text">
                        <h2><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h2>
                        <?php the_excerpt(); ?>
                    </div>
                </li>
            <?php endwhile; ?>
        </div>

<?php endif ?>


        <section id="cases">
            <?php
            $args = array('posts_per_page' => 3,'post_type' => 'post', 'category' => '-3', 'order' => 'ASC');

            $posts_array = get_posts( $args );

            foreach ( $posts_array as $post ): 
            ?>             
            <article>
                <div class="content-wrapper">
                    <div class="image">
                        <?php if (has_post_thumbnail()): ?><?php the_post_thumbnail(); ?><?php endif ?>
                    </div>
                    <div class="text">
                        <h2><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h2>
                        <?php the_excerpt(); ?>
                    </div>
                </div>
                <div class="link">  
                    <a href="<?php echo get_permalink(); ?>">Read More</a>
                </div>
            </article>
            <?php endforeach ?>
        </section>
    </div>

    <aside class="grid_8">
        <div id="checkpoints">
            <?php $page = get_page(6) ?>
            <h2><?php echo $page->post_title ?></h2>
            <p><?php echo $page->post_excerpt ?></p>
            <div class="link">  
                <a href="<?php echo $page->post_name ?>">Read More</a>
            </div>
        </div>
        <div id="voice">
            <h2>Have Your Voice Heard!</h2>
            <?php 
            $comments = get_comments(array('post_id' => '10', 'number'=>'2', 'status'=>'approve'));
            foreach($comments as $comment) :
            ?>
            <div class="article">
                <div class="image">
                    <?php echo get_avatar($comment->user_id) ?>
                </div>
                <div class="content-text">
                    <div><strong><?php echo $comment->comment_author ?></strong></div>
                    <p><?php echo $comment->comment_content ?></p>
                </div>
            </div>
            <?php endforeach ?>
            <div class="link">  
                <a href="have-your-voice-heard">Read More</a>
            </div>
        </div>
        <div id="twitter-box">
            <?php
            if (is_active_sidebar('homepage-twitter')) :
                dynamic_sidebar('homepage-twitter');
            endif;
            ?>
        </div>
    </aside>
</div>

<?php get_footer(); ?>    