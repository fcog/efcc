<?php
/*
Template Name: Home
*/
?>
<?php get_header(); ?>

    <div id="content" class="container_24">
        <?php echo do_shortcode('[nggallery id=1 template="sliderview"]'); ?>
        <section id="about">
            <?php $page = get_page_by_title('About Us') ?>
            <h3 class="about"><?php echo $page->post_title ?></h3>
            <p><?php echo $page->post_excerpt ?></p>
            <a href="<?php echo $page->post_name ?>">&raquo;</a>
        </section>
        <section id="news" class="grid_18">
            <h3 class="news">News</h3>
            <?php
            $args = array('posts_per_page' => 3,'post_type' => 'post');

            $posts_array = get_posts( $args );

            foreach ( $posts_array as $post ): 
            ?>            
                <article class="alpha">
                    <h4><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h4>
                    <?php the_excerpt(); ?>
                    <a href="<?php echo get_permalink(); ?>" class="read-more">Read more</a>
                </article>
            <?php endforeach ?>
        </section>
        <section id="events" class="grid_6">
            <h3 class="events">Events</h3>
            <?php
            $args = array(
                'post_type' => 'events',
                'posts_per_page' => 2,
                'meta_key' => 'event_date',
                'orderby' => 'meta_value',
                'order' => 'ASC',
                'meta_query' => array(
                    array(
                        'key' => 'event_date',
                        'value' => date('Ymd'),
                        'compare' => '>=',
                    )
                )
            );

            $the_query = new WP_Query( $args );


            if ( $the_query->have_posts() )
                while ( $the_query->have_posts() ):
                    $the_query->the_post();
            ?>       
                <?php $date = get_post_custom_values('event_date') ?>    
                <?php if (strtotime($date[0]) >= time()): ?>                     
                    <article>
                        <time><?php echo date("d m Y", strtotime($date[0])) ?></time>
                        <a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a>
                        <?php the_excerpt(); ?>
                    </article>
                <?php endif ?>
            <?php endwhile ?>
            <div class="white-bar"><a href="news-events">View all &raquo;</a></div>
        </section>
    </div>
    <section id="careers">
        <div id="ship">&nbsp;</div>
        <div class="container_24">
            <?php $page = get_page_by_title('Careers') ?>
            <h3 class="careers"><?php echo $page->post_title ?></h3>
            <p class="grid_17"><?php echo $page->post_excerpt ?></p>
            <a href="<?php echo $page->post_name ?>">&raquo;</a>
        </div>
    </section>

<script type="text/javascript">
function equalHeight(group) {
  var tallest = 0;
  group.each(function() {
    var thisHeight = $(this).height();
    if(thisHeight > tallest) {
      tallest = thisHeight;
    }
  });
  group.height(tallest+45);
}
$(document).ready(function() {
  equalHeight($("#news article"));
});
</script>    

<?php get_footer(); ?>    