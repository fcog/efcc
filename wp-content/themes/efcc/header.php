<!DOCTYPE html>
<html lang="en">
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie-7"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie-8"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">

    <!--jQuery-->
    <?php wp_head(); ?>

    <!--UI theme roller-->
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/css/custom-theme/jquery-ui-1.8.24.custom.css" />

    <!--Styles-->
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/css/960.css" />
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/style.css" />

    <!--prefix free-->
    <script src="<?php echo get_template_directory_uri(); ?>/js/prefixfree.min.js"></script>

    <!--Only Mac - Safari Class-->
    <script type="text/javascript">
    jQuery(function(){
        // console.log(navigator.userAgent);
        /* Adjustments for Safari on Mac */
        if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Mac') != -1) {
            jQuery('html').addClass('mac'); // provide a class for the safari-mac specific css to filter with
        }
    });
    </script>

    <?php  if (is_home()): ?>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.flexslider-min.js"></script>
    <link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/css/flexslider.css" />
    <script type="text/javascript" charset="utf-8">
        jQuery(window).load(function() {
            jQuery('#slideshow').flexslider({
                animation: "fade",
                direction: "horizontal",
                slideshowSpeed: 7000,
                animationSpeed: 600,
                directionNav: false, 
            });
        });
    </script>    
    <?php endif ?>

    <!--Selectivizr-->
    <!--[if (gte IE 6)&(lte IE 8)]>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/selectivizr.js"></script>
        <noscript><link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/theme.css" /></noscript>
    <![endif]-->

    <!--[if IE]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--[if gte IE 9]>
        <style type="text/css">
         .gradient {
            filter: none;
        }
     </style>
    <![endif]-->

	<title><?php echo get_bloginfo("name"); ?></title>

</head>
<body>
<div id="wrap">
    <header id="site-header">
        <div class="container_24">
            <h1 id="logo" class="grid_9">
                <a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/photos/logo.png" alt="EFCC Reality Checkpoints"></a>
            </h1>
            <div id="social-network" class="grid_7 prefix_8">
                <ul>
                    Follow us on:
                    <?php
                    if (is_active_sidebar('homepage-social-buttons')) :
                        dynamic_sidebar('homepage-social-buttons');
                    endif;
                    ?>
                </ul>
            </div>
        </div>
    </header>
    <div id="navigation">
        <div class="container_24">
            <nav class="grid_17">
                <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu ' ) ); ?>
            </nav>
            <div id="search" class="grid_7">
                <?php get_search_form(); ?>
            </div>
        </div> 
    </div>
    <div id="container" class="container_24">