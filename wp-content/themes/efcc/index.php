<?php get_header(); ?>

    <div id="primary" class="content-area">

        <?php if (get_post_type() == 'post' && is_active_sidebar( 'sidebar-news' )): ?>
            <div id="content" class="site-content has-sidebar" role="main">
        <?php elseif (get_post_type() == 'events' && is_active_sidebar( 'sidebar-events' )): ?>
            <div id="content" class="site-content has-sidebar" role="main">
        <?php else: ?>
            <div id="content" class="site-content" role="main">
        <?php endif ?>

        <?php if ( have_posts() ) : ?>
            <?php /* The loop */ ?>
            <?php while ( have_posts() ) : the_post(); ?>
                <?php get_template_part( 'content', get_post_type() ); ?>
            <?php endwhile; ?>

        <?php else : ?>
            <?php get_template_part( 'home', '' ); ?>
        <?php endif; ?>

        </div><!-- #content -->
        <?php if (get_post_type() == 'post') get_sidebar('news'); ?>
        <?php if (get_post_type() == 'events') get_sidebar('events'); ?>
    </div><!-- #primary -->
<?php get_footer(); ?>